export const option1 = {
    tooltip: {
        trigger: 'item',
    },
    series: [
        {
            name: '面积模式',
            type: 'pie',
            radius: [10, 60],
            center: ['50%', '50%'],
            roseType: 'area',
            itemStyle: {
                borderRadius: 5,
            },
            data: [
                { name: '天气预警', value: 100 },
                { name: '病虫害预警', value: 50 },
                { name: '任务预警', value: 130 },
                { name: '监测设备预警', value: 62 },
            ],
            label: {
                color: '#c0d1f2',
            },
        },
    ],
};
