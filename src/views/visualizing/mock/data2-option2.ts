import * as echarts from 'echarts';

export const option2 = {
	grid: {
		top: 10,
		right: 0,
		bottom: 20,
		left: 30,
	},
	tooltip: {
		trigger: 'axis',
		axisPointer: {
			type: 'shadow',
		},
	},
	xAxis: {
		data: ['1月', '2月', '3月', '4月', '5月', '6月'],
		axisLine: {
			lineStyle: {
				color: 'rgba(22, 207, 208, 0.5)',
				width: 1,
			},
		},
		axisTick: {
			show: false,
		},
		axisLabel: {
			color: '#c0d1f2',
		},
	},
	yAxis: [
		{
			type: 'value',
			axisLine: {
				show: true,
				lineStyle: {
					color: 'rgba(22, 207, 208, 0.1)',
				},
			},
			axisLabel: {
				color: '#c0d1f2',
			},
			splitLine: {
				show: true,
				lineStyle: {
					color: 'rgba(22, 207, 208, 0.3)',
				},
			},
			splitArea: {
				show: true,
				areaStyle: {
					color: 'rgba(22, 207, 208, 0.02)',
				},
			},
			nameTextStyle: {
				color: '#16cfd0',
			},
		},
		{
			type: 'value',
			position: 'right',
			axisLine: {
				show: false,
			},
			axisLabel: {
				show: true,
				formatter: '{value}%',
				textStyle: {
					color: '#16cfd0',
				},
			},
			splitLine: {
				show: false,
			},
			axisTick: {
				show: false,
			},
			splitArea: {
				show: true,
				areaStyle: {
					color: 'rgba(22, 207, 208, 0.02)',
				},
			},
			nameTextStyle: {
				color: '#16cfd0',
			},
		},
	],
	series: [
		{
			name: '销售水量',
			type: 'line',
			yAxisIndex: 1,
			smooth: true,
			showAllSymbol: true,
			symbol: 'circle',
			itemStyle: {
				color: '#058cff',
			},
			lineStyle: {
				color: '#058cff',
			},
			areaStyle: {
				color: 'rgba(5,140,255, 0.2)',
			},
			data: [4.2, 3.8, 4.8, 3.5, 2.9, 2.8],
		},
		{
			name: '主营业务',
			type: 'bar',
			barWidth: 15,
			itemStyle: {
				normal: {
					color: new echarts.graphic.LinearGradient(0, 0, 0, 1, [
						{
							offset: 0,
							color: '#00FFE3',
						},
						{
							offset: 1,
							color: '#4693EC',
						},
					]),
				},
			},
			data: [4.2, 3.8, 4.8, 3.5, 2.9, 2.8],
		},
	],
};
