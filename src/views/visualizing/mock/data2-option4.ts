import * as echarts from 'echarts';

export const option4 = {
	grid: {
		top: 10,
		right: 10,
		bottom: 20,
		left: 30,
	},
	tooltip: {
		trigger: 'axis',
	},
	xAxis: {
		type: 'category',
		boundaryGap: false,
		data: ['1月', '2月', '3月', '4月', '5月', '6月'],
		axisLine: {
			lineStyle: {
				color: 'rgba(22, 207, 208, 0.1)',
				width: 1,
			},
		},
		axisTick: {
			show: false,
		},
		axisLabel: {
			interval: 0,
			color: '#c0d1f2',
			textStyle: {
				fontSize: 10,
			},
		},
	},
	yAxis: [
		{
			type: 'value',
			axisLabel: {
				color: '#c0d1f2',
			},
			splitLine: {
				show: true,
				lineStyle: {
					color: 'rgba(22, 207, 208, 0.3)',
				},
			},
			splitArea: {
				show: true,
				areaStyle: {
					color: 'rgba(22, 207, 208, 0.02)',
				},
			},
			nameTextStyle: {
				color: '#16cfd0',
			},
		},
	],
	series: [
		{
			name: '温度',
			type: 'line',
			smooth: true,
			lineStyle: {
				width: 0,
			},
			areaStyle: {
				opacity: 0.8,
				color: new echarts.graphic.LinearGradient(0, 0, 0, 1, [
					{
						offset: 0,
						color: 'rgba(128, 255, 165)',
					},
					{
						offset: 1,
						color: 'rgba(1, 191, 236)',
					},
				]),
			},
			emphasis: {
				focus: 'series',
			},
			data: [140, 232, 101, 264, 90, 70],
		},
	],
};
