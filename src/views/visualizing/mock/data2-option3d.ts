import worldImg from '../images/world.jpg';
import bathymetryImg from '../images/bathymetry.jpg';

export interface IPoint {
	coords: [number, number][];
	value: string;
}

export const option3d = {
    globe: {
        baseTexture: worldImg,
        heightTexture: bathymetryImg,
        shading: 'realistic',
        light: {
            ambient: {
                intensity: 0.4,
            },
            main: {
                intensity: 0.4,
            },
        },
        viewControl: {
            autoRotate: true,
        },
        postEffect: {
            enable: true,
            bloom: {
                enable: true,
            },
        },
        globeRadius: 0,
    },
    series: {
        type: 'lines3D',
        coordinateSystem: 'globe',
        blendMode: 'lighter',
        lineStyle: {
            width: 1,
            color: 'rgb(50, 50, 150)',
            opacity: 0.1,
        },
        data: [] as IPoint[],
    },
};
