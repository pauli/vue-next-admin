import * as echarts from 'echarts';

export const option6 = {
	tooltip: {
		trigger: 'axis',
		axisPointer: {
			type: 'shadow',
		},
	},
	grid: {
		top: 20,
		right: 50,
		bottom: 0,
		left: 80,
	},
	xAxis: [
		{
			splitLine: {
				show: false,
			},
			type: 'value',
			show: false,
		},
	],
	yAxis: [
		{
			splitLine: {
				show: false,
			},
			axisLine: {
				//y轴
				show: false,
			},
			type: 'category',
			axisTick: {
				show: false,
			},
			inverse: true,
			data: ['施肥任务完成率', '施药任务完成率', '农事任务完成率'],
			axisLabel: {
				color: '#A7D6F4',
				fontSize: 12,
			},
		},
	],
	series: [
		{
			name: '标准化',
			type: 'bar',
			barWidth: 10, // 柱子宽度
			label: {
				show: true,
				position: 'right', // 位置
				color: '#A7D6F4',
				fontSize: 12,
				distance: 15, // 距离
				formatter: '{c}%', // 这里是数据展示的时候显示的数据
			}, // 柱子上方的数值
			itemStyle: {
				barBorderRadius: [0, 20, 20, 0], // 圆角（左上、右上、右下、左下）

				color: new echarts.graphic.LinearGradient(
					1,
					0,
					0,
					0,
					[
						{
							offset: 0,
							color: '#51C5FD',
						},
						{
							offset: 1,
							color: '#005BB1',
						},
					],
					false
				), // 渐变
			},
			data: [75, 100, 60],
		},
	],
};
