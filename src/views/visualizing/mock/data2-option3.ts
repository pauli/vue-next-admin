export const option3 = {
    grid: {
        top: 10,
        right: 0,
        bottom: 20,
        left: 30,
    },
    tooltip: {
        trigger: 'axis',
    },
    xAxis: {
        data: ['1月', '2月', '3月', '4月', '5月', '6月'],
        axisLine: {
            lineStyle: {
                color: 'rgba(22, 207, 208, 0.1)',
                width: 1,
            },
        },
        axisTick: {
            show: false,
        },
        axisLabel: {
            color: '#c0d1f2',
        },
    },
    yAxis: [
        {
            type: 'value',
            axisLine: {
                show: true,
                lineStyle: {
                    color: 'rgba(22, 207, 208, 0.1)',
                },
            },
            axisLabel: {
                color: '#c0d1f2',
            },
            splitLine: {
                show: true,
                lineStyle: {
                    color: 'rgba(22, 207, 208, 0.3)',
                },
            },
            splitArea: {
                show: true,
                areaStyle: {
                    color: 'rgba(22, 207, 208, 0.02)',
                },
            },
            nameTextStyle: {
                color: '#16cfd0',
            },
        },
    ],
    series: [
        {
            name: '预购队列',
            type: 'line',
            data: [200, 85, 112, 275, 305, 415],
            itemStyle: {
                color: '#16cfd0',
            },
        },
        {
            name: '最新成交价',
            type: 'line',
            data: [50, 85, 22, 155, 170, 25],
            itemStyle: {
                color: '#febb50',
            },
        },
    ],
};
